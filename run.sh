#!/bin/bash

POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_DATABASE=raster

for raster_directory in `find . -maxdepth 1 -type d -name 'raster_*'`;do

  name=${raster_directory//.\/}

  # Caso já tenha executado, ignora
  if grep -q $name .lock;then
    continue;
  fi

  raster2pgsql -s 32725 -C -l 2,4 -I -F -t auto \
   $raster_directory/*.tif public.$name | \
    PGPASSWORD=$POSTGRES_PASSWORD \
    psql -h $POSTGRES_HOST \
    -p $POSTGRES_PORT \
    -U $POSTGRES_USER \
    -d $POSTGRES_DATABASE

  echo $name >> .lock

done